(use-package elementaryx-ox)

(use-package ox-publish
  :commands (org-publish org-publish-all org-publish-file org-publish-org-to org-publish-project)
  :config
  ;; Verbose errors for pdf generation
  ;; https://www.pank.eu/blog/blog-setup.html
  ;; Note: we use /dev/tty instead of /dev/stdout: https://stackoverflow.com/questions/16747449/runuser-permission-denied-on-dev-stdout
  ;; Note: 2022/12/20: We shall even use "$(tty)" for more portability (thanks Antoine Jego)
  (defun elementaryx/org-latex-publish-to-pdf-verbose-on-error (plist filename pub-dir)
    (condition-case err
	(org-latex-publish-to-pdf
	 plist filename pub-dir)
      ;; Handle Org errors at first.
      (user-error
       (message "%s" (error-message-string err)))
      ;; If the Org source is fine, handle LaTeX errors.
      (error
       (with-current-buffer "*Org PDF LaTeX Output*"
	 (message (buffer-string))))))
  (defun elementaryx/org-beamer-publish-to-pdf-verbose-on-error (plist filename pub-dir)
    (condition-case err
	(org-beamer-publish-to-pdf
	 plist filename pub-dir)
      ;; Handle Org errors at first.
      (user-error
       (message "%s" (error-message-string err)))
      ;; If the Org source is fine, handle LaTeX errors.
      (error
       (with-current-buffer "*Org PDF LaTeX Output*"
	 (message (buffer-string))))))
    ;; Default org-publish-project-alist for elementaryx
  (defvar elementaryx/org-publish-project-alist
    (list
     (list "site-org"
           :base-directory "."
           :base-extension "org"
           :recursive t
           :exclude (regexp-opt '("draft" "include" "rawlatex" "public" "sitemap" "slides" "static"))
           :publishing-function '(elementaryx/org-latex-publish-to-pdf-verbose-on-error org-html-publish-to-html)
           :publishing-directory "./public"
           :auto-sitemap t
           :sitemap-filename "./sitemap-org.inc"
           :makeindex t
           :author "compose development team"
           :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")
     (list "site-org-attach"
           :base-directory "."
           :base-extension "py\\|ipynb\\|css\\|cpp\\|h\\|png\\|csv\\|txt\\|svg\\|jpeg\\|yml\\|el\\|setup"
           :recursive t
           :exclude (regexp-opt '("draft" "include" "rawlatex" "public" "sitemap" "static"))
           :publishing-function '(org-publish-attachment)
           :publishing-directory "./public/"
           :auto-sitemap t
           :sitemap-filename "./sitemap-org-attach.inc")
     (list "site-static"
           :base-directory "./static"
           :base-extension "png\\|csv\\|txt\\|pdf\\|svg\\|jpeg\\|yml\\|el\\|setup"
           :recursive t
           :publishing-function '(org-publish-attachment)
           :publishing-directory "./public/static/")
     (list "slides"
           :base-directory "./slides"
           :base-extension "org"
           :recursive t
           :exclude (regexp-opt '("draft" "include"))
           :publishing-function '(elementaryx/org-beamer-publish-to-pdf-verbose-on-error org-re-reveal-publish-to-reveal org-re-reveal-publish-to-reveal-client)
           :publishing-directory "./public/slides"
           :auto-sitemap t
           :sitemap-filename "../sitemap-org-slides.inc")
     (list "site-sitemap"
           :base-directory "sitemap"
           :base-extension "org"
           :recursive nil
           :publishing-function '(org-html-publish-to-html)
           :publishing-directory "./public/sitemap"
           :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")
     (list "site" :components '("site-org" "site-org-attach" "site-static" "slides" "site-sitemap")))
    "org-publish-project-alist template for elementary")
  ;; Set org-publish-project-alist accordingly
  (setq org-publish-project-alist elementaryx/org-publish-project-alist)
  :custom
  ;; Force publishing of unchanged files to make sure all the pages get published.
  ;; Otherwise, the files considered unmodified based on Org timestamps are not
  ;; published even if they were previously deleted from the publishing directory.
  (org-publish-use-timestamps-flag nil))

(provide 'elementaryx-ox-publish)
